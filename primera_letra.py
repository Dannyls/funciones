#___Programa "Double char"
#___Autor:"Danny Lima"____
#___Email:danny.lima@unl.edu.ec

def first_letter(letter):
    number_letter = letter[0]
    return number_letter


if __name__ == "__main__":
    usuario = input("Ingrese la palabra deseada:")
    print(first_letter(usuario))
