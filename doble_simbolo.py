#___Programa "Doble simbolo"
#___Autor:"Danny Lima"____
#___Email:danny.lima@unl.edu.ec

def double_char(cadena):
    tamaño = len(cadena)
    i = 0
    while i < tamaño:
        print(2 * cadena[i], end="")
        i += 1


palabra = input("Ingrese una palabra: ")
double_char(palabra)
