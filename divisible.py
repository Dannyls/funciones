#___Programa para saber si un número es divisible
#___Autor:"Danny Lima"____
#___Email:"danny.lima@unl.edu.ec"

def divisible_by(number, divisor):
    if number % divisor == 0:
        return True
    else:
        return False


if __name__ == "__main__":
    number = int(input("Ingrese el número a dividir: "))
    divisor = int(input("Ingrese el divisor: "))
    print(divisible_by(number, divisor))
