#___Programa "Double char"
#___Autor:"Danny Lima"____
#___Email:danny.lima@unl.edu.ec

def double_char(doblee):
    doble_letra = []
    for letra in doblee:
        doble_letra.append(letra + letra)
    return ("".join(doble_letra))


if __name__ == "__main__":
    palabra_ingreso = input("Ingrese una palabra")
    print(double_char(palabra_ingreso))